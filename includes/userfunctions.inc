<?php

/**
 * @file
 * UserFunctions class including all user-related functions.
 */

namespace extraTools;

class UserFunctions {

  /**
   * Function updateTestEmails.
   *
   * Update all users to have a new email address incremented with a number, eg test+1@example.com.
   *
   * @param string $name
   *   The first part of the email eg --name=test for test@example.com.
   * @param string $domain
   *   The email domain, eg --domain=example.com for test@example.com.
   * @param string $suffix
   *   The email suffix, eg --suffix=- for test-1@example.com, defaults to +.
   * @param string $noadmin
   *   If set to 0 the user 1 account email address will also be reset, if set
   *   to 1 the User 1 account will not be reset. This defaults to 1.
   * @param bool $inc_commerce
   *   Does the user want drupal commerce order emails to also be updated.
   *   Defaults to FALSE.
   *
   * @return array
   *   Return either a success or failure message in an array. The calling
   *   function will need to read the array to see if it failed or succeeded
   */
  public static function updateTestEmails($name, $domain, $suffix, $noadmin, $inc_commerce = FALSE) {
    $message = array();
    $where = '';
    // If params haven't been passed, reset the defaults here.
    // Set the default suffix.
    if (!isset($suffix)) {
      $suffix = '+';
    }
    // Set the default noadmin settings.
    if (!isset($noadmin)) {
      $noadmin = 1;
    }

    if ($noadmin) {
      $where .= ' where uid not in (0, 1);';
    }
    else {
      $where .= ' where uid not in (0);';
    }

    $sql = 'UPDATE users SET mail = ' . 'CONCAT("' . $name . $suffix . '",(@ordering := @ordering + 1) , "@' . $domain . '")' . ' ' . $where;

    $result = _extra_tools_execute_sql_with_increment($sql);
    if (is_array($result) && isset($result['error'])) {
      return $result;
    }

    if ($result) {
      // Get the total number of users.
      $num_updated = _extra_tools_get_users_total($noadmin);
      $message['success'][] = t('Success! @total reset.', array(
        '@total' => format_plural($num_updated, '1 user email', '@count user emails'),
      ));
    }
    else {
      $message['error'] = t("DB Error, could not query the database\n'MySQL Error: %error'", array('%error' => mysql_error()));
      return $message;
    }

    if ($inc_commerce) {
      if (module_exists('commerce')) {
        // Do the same for commerce orders.
        $sql = 'UPDATE commerce_order SET  mail = ' . 'concat("' . $name . $suffix . '",(@ordering := @ordering + 1) , "@' . $domain . '") ' . $where;

        $result = _extra_tools_execute_sql_with_increment($sql);
        if (is_array($result) && isset($result['error'])) {
          return $result;
        }

        if ($result) {
          $message['success'][] = t('Success! All Order Emails Reset');
        }
        else {
          $message['error'] = t("DB Error, could not query the database\n'MySQL Error: %error'", array('%error' => mysql_error()));
          return $message;
        }

        // And the revisions.
        if ($noadmin) {
          $where = ' where order_id in (select order_id from commerce_order where uid not in (0, 1));';
        }
        else {
          $where = ' where order_id in (select order_id from commerce_order where uid not in (0));';
        }

        $sql = 'UPDATE commerce_order_revision SET  mail = ' . 'concat("' . $name . $suffix . '",(@ordering := @ordering + 1) , "@' . $domain . '") ' . $where;

        $result = _extra_tools_execute_sql_with_increment($sql);
        if (is_array($result) && isset($result['error'])) {
          return $result;
        }

        if ($result) {
          $message['success'][] = t('Success! All Order Revision Emails Reset');
        }
        else {
          $message['error'] = t("DB Error, could not query the database\n'MySQL Error: %error'", array('%error' => mysql_error()));
          return $message;
        }
      }
    }
    return $message;
  }

  /**
   * Function updateSetPasswords.
   *
   * Update all users except 0 and 1 with the same password.
   *
   * @param string $userpwd
   *   The password to set.
   * @param bool $mailpass
   *   Email each user to let them know the password has been reset.
   *
   * @return array
   *   Indicating success of failure including success or failure messages.
   */
  public static function updateSetPasswords($userpwd, $mailpass = 0) {
    require_once './includes/password.inc';
    $num_updated = db_update('users')
      ->fields(array(
        'pass' => user_hash_password($userpwd),
      ))
      ->condition('uid', array(0, 1), 'NOT IN')
      ->execute();
    if ($num_updated) {
      $message['success'][] = t('Success! !num_updated User Passwords Reset.', array('!num_updated' => $num_updated));
    }
    else {
      $message['error'] = t("DB Error, could not query the database\n'MySQL Error: %error'", array('%error' => mysql_error()));
      return $message;
    }

    if ($mailpass) {
      self::sendUpdatePasswordEmail($message, $userpwd);
    }

    return $message;
  }

  /**
   * Function updateRandomPasswords.
   *
   * Update all users except 0 and 1 with a randomly generated password.
   *
   * @param bool $mailpass
   *   Email each user with a login link.
   *
   * @return array
   *   Error and or success messages to be printed to the user by the calling
   *   function.
   */
  public static function updateRandomPasswords($mailpass = 0) {
    $count = 0;
    $query = db_select('users', 'u');
    $query->fields('u', array('uid'));
    $query->condition('uid', array(0, 1), 'NOT IN');
    $result = $query->execute();
    while ($record = $result->fetchAssoc()) {
      $myuid = $record['uid'];
      // Get the user object.
      $myuser = user_load($myuid);
      // Create  a new password.
      $mypass = user_password();
      // Create the edit array.
      $myedits = array(
        'pass' => $mypass,
      );
      // Save the user account.
      if (user_save($myuser, $myedits)) {
        $count += 1;
      }
      else {
        $message['error'][] = t('User update failed for user !uid', array('!uid' => $record->uid));
      }
    }
    if ($count) {
      $message['success'][] = t('Success! !count User Passwords Reset.', array('!count' => $count));
    }
    if ($mailpass) {
      self::sendUpdatePasswordEmail($message);
    }

    return $message;
  }

  /**
   * Function updateTestUsernames.
   *
   * Update all users to have a new username incremented with a number, eg test+1.
   *
   * @param string $name
   *   The new username eg --name=test for test.
   * @param string $suffix
   *   The username suffix, eg --suffix=- for test-1, defaults to +.
   * @param string $noadmin
   *   If set to 0 the user 1 account username will also be reset, if set
   *   to 1 the User 1 account will not be reset. This defaults to 1.
   *
   * @return array
   *   Return either a success or failure message in an array. The calling
   *   function will need to read the array to see if it failed or succeeded
   */
  public static function updateTestUsernames($name, $suffix, $noadmin) {
    $message = array();
    $where = '';
    // If params haven't been passed, reset the defaults here.
    // Set the default suffix.
    if (!isset($suffix)) {
      $suffix = '+';
    }
    // Set the default noadmin settings.
    if (!isset($noadmin)) {
      $noadmin = 1;
    }

    if ($noadmin) {
      $where .= ' where uid not in (0, 1);';
    }
    else {
      $where .= ' where uid not in (0);';
    }

    $sql = 'UPDATE users SET name = ' . 'CONCAT("' . $name . $suffix . '",(@ordering := @ordering + 1)) ' . $where;

    $result = _extra_tools_execute_sql_with_increment($sql);
    if (is_array($result) && isset($result['error'])) {
      return $result;
    }

    if ($result) {
      // Get the total number of users.
      $num_updated = _extra_tools_get_users_total($noadmin);
      $message['success'][] = t('Success! @total reset.', array(
        '@total' => format_plural($num_updated, '1 username', '@count usernames'),
      ));
    }
    else {
      $message['error'] = t("DB Error, could not query the database\n'MySQL Error: %error'", array('%error' => mysql_error()));
      return $message;
    }

    return $message;
  }

  /**
   * Function sendUpdatePasswordEmail.
   *
   * Send the 'Update password' email.
   *
   * @param string $message
   *   The email message.
   * @param string $userpwd
   *   The user password.
   */
  private static function sendUpdatePasswordEmail(&$message, $userpwd = '') {

    global $language;
    $num_updated = 0;

    $user_list = db_select('users', 'u')
      ->fields('u', array('uid'))
      ->condition('uid', array(0, 1), 'NOT IN')
      ->execute();

    if ($userpwd != '') {
      $from_name = variable_get('site_name', '');
      $from_email = variable_get('site_mail', '');
      $subject = 'Password Reset';

      $params = array(
        'from_name' => $from_name,
        'from_email' => $from_email,
        'subject' => $subject,
        'body' => t('You password has been reset by the system administrator. Your new password is !pwd', array('!pwd' => $userpwd)),
      );
    }

    while ($userpass = $user_list->fetchAssoc()) {
      $account = user_load($userpass['uid']);
      if ($account) {
        $num_updated += 1;
        if ($userpwd == '') {
          // Mail one time login URL and instructions using current language.
          $mail = _user_mail_notify('password_reset', $account, $language);
        }
        else {
          $to_name = $account->name;
          $to_email = $account->mail;
          $params['to_name'] = $to_name;
          $params['to_email'] = $to_email;
          drupal_mail('extra_tools', 'reset', $to_email, $language, $params, $from_email);
        }

        if (!empty($mail)) {
          watchdog('user', 'Password reset instructions mailed to %name at %email.', array('%name' => $account->name, '%email' => $account->mail));
        }
      }
    }

    if ($num_updated) {
      $message['success'][] = t('Success! !num_updated User Passwords Reset Emails sent.', array('!num_updated' => $num_updated));
    }
  }

}
