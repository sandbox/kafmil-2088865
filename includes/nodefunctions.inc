<?php

/**
 * @file
 * NodeFunctions class including all node-related functions.
 */

namespace extraTools;

class NodeFunctions {

  /**
   * Function exportNodes.
   *
   * Export all nodes into the specified file.
   *
   * @param string $node_types
   *   An string of node types to export.
   * @param string $file
   *   The name of the file where the the exported content will be saved.
   *
   * @return bool
   *   Success or failure message.
   */
  public static function exportNodes($node_types, $file) {
    // Get the valid node types.
    $valid_node_types = node_type_get_types();

    // Split the node types into an array.
    $node_types = explode(', ', $node_types);

    // Make sure all elements are unique.
    $node_types = array_unique($node_types);

    // Build up the output array.
    $output = array();

    // Loop through all the $node_types options.
    foreach ($node_types as $key => $node_type) {
      // Make sure the provided node types are valid.
      if (!array_key_exists($node_type, $valid_node_types)) {
        unset($node_types[$key]);
        $output[] = t('Invalid type "@type" removed from the list', array('@type' => $node_type));
      }
    }

    // In case no valid node types were provided, return all the valid ones instead.
    if (empty($node_types)) {
      $node_types = array_keys($valid_node_types);
    }

    // At this point we know that node types have only valid results, so create the relevant files for each node type.
    foreach ($node_types as $node_type) {
      $node_file = $file . $node_type . '.txt';

      // Load all relevant nodes.
      $temp = (node_load_multiple(array(), array('type' => $node_type)));

      $nodesfile = '';
      $line_count = 0;

      foreach ($temp as $node) {
        $nodesfile .= serialize($node) . '<drush-sep>';
        $line_count++;
      }

      $nodesfile = substr($nodesfile, 0, strlen($nodesfile) - 11);
      if ($nodesfile) {
        file_put_contents($node_file, $nodesfile);
      }
      if ($line_count > 0) {
        $output[] = t('Exported @type content to @file: @count', array(
          '@type' => $node_type,
          '@file' => $node_file,
          '@count' => format_plural($line_count, '1 node', '@count nodes')
        ));
      }
    }

    // No nodes were exported.
    if (empty($output)) {
      return t('No nodes found for node types: @types', array('@types' => implode(', ', $node_types)));
    }

    return implode("\n", $output);
  }

  /**
   * Function importNodes.
   *
   * Import all nodes from the specified file into the system.
   *
   * @param string $file
   *   A comma-delimited list of file paths.
   *
   * @return string
   *   Success or failure message.
   */
  public static function importNodes($file) {
    $error_valid_file_required = 'Path @file invalid. A valid file path must be provided. Make sure you are in the correct folder or provide a full path to where the file sits. Additionally make sure the file is not empty. No nodes were imported.';

    // Form an array of all file paths.
    $files = explode(', ', $file);

    // Build up the output.
    $output = array();

    // Loop through each path.
    foreach ($files as $file) {
      // Work out if the user has entered a relative file name or a full path).
      $explode = explode('/', $file);
      // A single file name was provided (i.e. not a full path).
      if (count($explode) == 1) {
        // Make sure the file path is complete (relative to where the user executes the command).
        $explode = explode('/', $_SERVER['PWD']);
        // Add the file name to the file path array.
        $explode[] = $file;
        // Finally rebuild the file path up again.
        $file = implode('/', $explode);
      }

      // If no file path is supplied or the file does not exist.
      if ($file == '' || !file_exists($file)) {
        return t($error_valid_file_required, array('@file' => $file));
      }

      // Get the file size.
      $file_size = filesize($file);

      // Open the file.
      $fp = @fopen($file, 'r');

      // If the file couldn't be opened for whatever reason, or the file is empty.
      if (!$fp || !$file_size) {
        return t($error_valid_file_required, array('@file' => $file));
      }
      // At this point we can assume all files are valid, so start the import process.
      else {
        // Get the contents of the file.
        $read_file = fread($fp, $file_size);

        // Get each node from the file.
        $array = explode('<drush-sep>', $read_file);

        $type = '';

        if (!empty($array)) {
          $total_nodes = 0;

          // Loop through the nodes.
          foreach ($array as $nodestring) {
            // Get the node.
            $node = unserialize($nodestring);
            // Get the node type.
            $type = $node->type;
            // Since this is a new node we need to unset the unique values from the exported node.
            unset($node->nid);
            unset($node->vid);
            // Prepare the node object.
            node_object_prepare($node);
            // Make sure the UID is set.
            $node->uid = 1;
            // Finally save it.
            node_save($node);
            // A valid node was found.
            if ($node->nid) {
              // Increment the number of nodes by 1.
              $total_nodes++;
            }
          }

          // Return the final message to the user, indicating how many nodes were created.
          $output[] = t('@total created.', array(
            '@total' => format_plural($total_nodes, '1 @type node', '@count @type nodes', array(
              '@type' => $type)
            ),
          ));
        }
      }
    }

    // No nodes were exported.
    if (empty($output)) {
      return t('No nodes created.');
    }

    return implode("\n", $output);

  }

}
