<?php
/**
 * @file
 * Contains ExtraTools.
 *
 * The extra_tools module drush integration.
 */

/**
 * Implements hook_drush_command().
 */
function extra_tools_drush_command() {
  $items = array();

  $items['extra_tools-test-emails'] = array(
    'description' => "Update all users to have a new email address incremented with a number, eg test+1@example.com.",
    'drupal dependencies' => array('extra_tools'),
    'options' => array(
      'name' => 'The first part of the email eg --name=test for test@example.com',
      'domain' => 'The email domain, eg --domain=example.com for test@example.com',
      'suffix' => 'The email suffix, eg --suffix=- for test-1@example.com, defaults to +',
      'noadmin' => 'If set to 0 the user 1 account email address will also be reset, if set to 1 the User 1 account will not be reset. This defaults to 1',
      'inc_commerce' => 'Include commerce orders, eg --inc_commerce=1 to udpate all commerce order emails at the same time.',
    ),
    'aliases' => array('exum', 'extra_tools'),
  );

  $items['extra_tools-user-pwds'] = array(
    'description' => "Update all users with new passwords.",
    'drupal dependencies' => array('extra_tools'),
    'options' => array(
      'userpass' => 'The user password to use for all users.',
      'genpass' => '1 to Generate a random password for each user. Defaults to 1 if userpass is left blank, otherwise defaults to 0.',
      'mailpass' => '1 to Email the user to inform them there password has been udpated. Defaults to 0.',
    ),
    'aliases' => array('exup', 'extra_tools'),
  );
  $items['extra_tools-update-user-usernames'] = array(
    'description' => "Update all users to have a new username incremented with a number, eg test+1.",
    'drupal dependencies' => array('extra_tools'),
    'options' => array(
      'name' => 'The new username eg --name=test for test',
      'suffix' => 'The username suffix, eg --suffix=- for test-1, defaults to +',
      'noadmin' => 'If set to 0 the user 1 account username will also be reset, if set to 1 the User 1 account will not be reset. This defaults to 1',
    ),
    'aliases' => array('etupusersnames', 'extra_tools'),
  );
  $items['extra_tools-fe-node-export'] = array(
    'description' => "Create a drush node export file.",
    'options' => array(
      'result-file' => 'Optional. The name you want to give the file (which will be appended with the node type) which can include the full path excluding the extension e.g. --result-file="export_" for export_[type].txt. Omitting this will result in the file being saved in the current folder with the name of the node type e.g. page.txt for Page nodes.',
      'node-types' => 'Optional. Type of node/s to export, delimited by a comma e.g. --node-types="article, page" for Articles and Pages. Defaults to all available node types.',
    ),
    'drupal dependencies' => array('extra_tools'),
    'aliases' => array('etnodeex'),
  );
  $items['extra_tools-fe-node-import'] = array(
    'description' => "Import nodes from an exported file.",
    'options' => array(
      'in-file' => 'Required. Path to the export file/s. Provide multiple paths delimited by a comma e.g. --in-file="page.txt, /full/path/to/article.txt"',
    ),
    'aliases' => array('etnodeim', 'extra_tools'),
  );
  $items['extra_tools-reset-field'] = array(
    'description' => "Update a field value on a specified entity.",
    'drupal dependencies' => array('extra_tools'),
    'options' => array(
      'entity' => 'The entity to change',
      'field' => 'The field to change',
      'search' => 'The current value',
      'replace' => 'The value to replace it with',
    ),
    'aliases' => array('erf', 'extra_tools'),
  );
  $items['extra_tools-make-menus'] = array(
    'description' => "UCreate the menus during the site migration.",
    'drupal dependencies' => array('extra_tools'),
    'options' => array(),
    'aliases' => array('amm', 'extra_tools'),
  );
  $items['extra_tools-test-context'] = array(
    'description' => "Import a test context.",
    'drupal dependencies' => array('extra_tools'),
    'options' => array(
      'file' => 'The file storing the context text',
    ),
    'aliases' => array('imc', 'extra_tools'),
  );
  $items['extra_tools-authorize-tokens'] = array(
    'description' => "Import a authorize.net tokens.",
    'drupal dependencies' => array('extra_tools'),
    'options' => array(
      'file' => 'Import authorize.net tokens from Marc',
    ),
    'aliases' => array('sat', 'extra_tools'),
    'aliases' => array('etni'),
  );

  $update_nodes_options = _extra_tools_update_nodes_options();
  $items['extra_tools-update-nodes'] = array(
    'description' => "Update all or certain nodes.",
    'drupal dependencies' => array('extra_tools'),
    'options' => array(
      'action' => 'Required. The action you want to execute. Possible options are ' . implode(', ', $update_nodes_options) . ' e.g. --action=publish to Publish.',
      'nids' => 'Optional. A comma delimited list of nids e.g. --nids=1,10 for nids 1 and 10. If omitted then ALL nodes will be updated.',
    ),
    'aliases' => array('etupnodes'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function extra_tools_drush_help($section) {
  switch ($section) {
    case 'drush:extra_tools-test-emails':
      return dt("Update all users to have a new email address incremented with a number, eg test+1@example.com. This is useful when moving a production db back to dev for testing.\nname: The first part of the email eg --name=test for test@example.com\ndomain: The email domain, eg --domain=example.com for test@example.com\nsuffix: The email suffix, eg --suffix=- for test-1@example.com, defaults to +\nnoadmin: If set to 0 the user 1 account email address will also be reset, if set to 1 the User 1 account will not be reset. This defaults to 1.\ninc_commerce: Include commerce orders, eg --inc_commerce=1 to update all commerce order emails at the same time.");

    case 'drush:extra_tools-user-pwds':
      return dt("Update all users with new passwords. The Users 0 and 1 password will not be changed.\nuserpass: The user password to use for all users. eg --userpass=Password1\ngenpass: Generate a random password for each user. Defaults to 1 if userpass is left blank, otherwise defaults to 0.\nmailpass: Email the user to inform them there password has been updated. Defaults to 0.");

    case 'drush:extra_tools-fe-node-export':
      return dt("Export nodes to a file (generally for use with import command).");

    case 'drush:extra_tools-fe-node-import':
      return dt("Import nodes from an exported file.");

    case 'drush:extra_tools-reset-field':
      return dt("Update a field value on a specified entity.\n Options:\n* entity: The entity to change eg --entity=User for the user entity");

    case 'drush:extra_tools-make-menus':
      return dt("Create the menus during the site migration");

    case 'drush:extra_tools-reset-field':
      return dt("Update a field value on a specified entity.\n Options:\n* entity: The entity to change eg --entity=User for the user entity");

    case 'drush:extra_tools-update-nodes':
      return dt("Update all or certain nodes.");
  }
}

/* drush import extra fields onto user from csv
open the csv and start reading, the first line will contain the headers
if they are not in good format and don't start with field then reject the whole import
    allow a force import option, then display a warning that this may cause problems and ask for
    confirmation
create an array with the key being the fieldname and the value being the array key within the csv
read the other lines of the csv then use csvarray[fieldarray['field_name']] to get the relevant
values
then use user_edit_array = array('fieldname' => array(LANGUAGE_NONEarray(array('value' => csvarray[fieldarray['field_name']])));
user_save($userobject,user_edit_array);*/

/**
 * Function drush_extra_tools_update_test_emails.
 *
 * Update all prod users with new usernames and passwords.
 */
/**
 * Function drush_extra_tools_test_emails.
 *
 * Reset all of the user emails (expect 0 and 1) to the requested email. This
 * will generate email based ona single users email address using a suffix eg test+1@mail.com.
 *
 * @return string
 *   An error or success message.
 */
function drush_extra_tools_test_emails() {
  // Get the variables from the user.
  $name = drush_get_option(array('name'));
  $domain = drush_get_option(array('domain'));
  $suffix = drush_get_option(array('suffix'));
  $noadmin = drush_get_option(array('noadmin'));
  $inc_commerce = drush_get_option(array('inc_commerce'));

  // This prob isn't required but I'm setting it specifically to FALSE if it is
  // unset.
  if (!isset($inc_commerce)) {
    $inc_commerce = FALSE;
  }

  // Check that the required fields have been passed in.
  if (!isset($name)) {
    return drush_set_error('Update test emails', 'The name value must be set!');
  }
  else {
    if (!isset($domain)) {
      return drush_set_error('Update test emails', 'The domain value must be set!');
    }
    else {
      // Include the class file.
      require_once 'includes/userfunctions.inc';
      // Call the function to reset the emails.
      $return = \extraTools\UserFunctions::updateTestEmails(
        $name,
        $domain,
        $suffix,
        $noadmin,
        $inc_commerce
      );

      // For each success message print it to the screen.
      if (isset($return['success'])) {
        foreach ($return['success'] as $success) {
          drush_print($success);
        }
      }
      // If there was a failure report it.
      if (isset($return['error'])) {
        return drush_set_error('Update test emails', $return['error']);
      }
    }
  }
}

/**
 * Update all prod users with new usernames and passwords.
 */
function drush_extra_tools_user_pwds() {
  // Inlcude the class file.
  require_once 'includes/userfunctions.inc';
  // Get all of the passed in parameters.
  $userpass = drush_get_option(array('userpass'));
  $genpass = drush_get_option(array('genpass'));
  $mailpass = drush_get_option(array('mailpass'));

  // If gepass is not set, see if the userpass is empty, if so, assume that
  // the user wants random passwords.
  if (!isset($genpass) || $genpass !== 1) {
    if (!isset($userpass)) {
      $genpass = 1;
    }
  }
  elseif ($genpass !== 0) {
    $genpass = 1;
  }

  // iF mailpass is not set, set it to 0.
  if (!isset($mailpass)) {
    $mailpass = 0;
  }

  // If the user wants to generate random passwords call the function to do that
  // Otherwise call the simple update password function.
  if ($genpass) {
    $return = \extraTools\UserFunctions::updateRandomPasswords(
      $mailpass
    );
  }
  else {
    $return = \extraTools\UserFunctions::updateSetPasswords(
      $userpass,
      $mailpass
    );
  }

  // Print all of the success messages.
  if (isset($return['success'])) {
    foreach ($return['success'] as $success) {
      drush_print($success);
    }
  }
  // Print all of the failure messages.
  if (is_array(($return['error']))) {
    $reterror = '';
    foreach ($return['error'] as $error) {
      $reterror .= $error . "\n";
    }
    return drush_set_error('Update test emails', $reterror);
  }
}

/**
 * Update all users usernames.
 */
function drush_extra_tools_update_user_usernames() {
  // Get the variables from the user.
  $name = drush_get_option(array('name'));
  $suffix = drush_get_option(array('suffix'));
  $noadmin = drush_get_option(array('noadmin'));

  // Check that the required fields have been passed in.
  if (!isset($name)) {
    return drush_set_error('Update test usernames', 'The name value must be set!');
  }
  else {
    // Include the class file.
    require_once 'includes/userfunctions.inc';
    // Call the function to reset the emails.
    $return = \extraTools\UserFunctions::updateTestUsernames(
      $name,
      $suffix,
      $noadmin
    );

    // For each success message print it to the screen.
    if (isset($return['success'])) {
      foreach ($return['success'] as $success) {
        drush_print($success);
      }
    }
    // If there was a failure report it.
    if (isset($return['error'])) {
      return drush_set_error('Update test usernames', $return['error']);
    }
  }
}

/**
 * Create a drush node export csv file.
 */
function drush_extra_tools_fe_node_export() {
  $node_types = drush_get_option(array('node-types'));
  $file = drush_get_option(array('result-file'));

  require_once 'includes/nodefunctions.inc';

  $export = \extraTools\NodeFunctions::exportNodes($node_types, $file);

  // If an error was returned, display it.
  if (isset($export['error'])) {
    drush_set_error(dt($export['error']));
  }
  // Otherwise a regular message should be returned.
  else {
    drush_print($export);
  }
}

/**
 * Import nodes from a csv file.
 */
function drush_extra_tools_fe_node_import() {
  if ($file = drush_get_option(array('in-file'))) {
    require_once 'includes/nodefunctions.inc';

    $import = \extraTools\NodeFunctions::importNodes($file);

    // If an error was returned, display it.
    if (isset($import['error'])) {
      drush_set_error(dt($import['error']));
      // Otherwise a regular message should be returned.
    }
    else {
      drush_print($import);
    }
  }
  else {
    drush_set_error('', 'An input file must be provided using the option --in-file. eg drush extra_tools-fe-node-import --in-file="myfile.txt"');
  }
}

/* drush import extra fields onto user from csv
open the csv and start reading, the first line will contain the headers
if they are not in good format and don't start with field then reject the whole import
    allow a force import option, then display a warning that this may cause problems and ask for
    confirmation
create an array with the key being the fieldname and the value being the array key within the csv
read the other lines of the csv then use csvarray[fieldarray['field_name']] to get the relevant
values
then use user_edit_array = array('fieldname' => array(LANGUAGE_NONEarray(array('value' => csvarray[fieldarray['field_name']])));
user_save($userobject,user_edit_array);*/


/**
 * Update a field on an entity with a new value based on another value.
 */
function drush_extra_tools_reset_field() {
  $entitytype = drush_get_option(array('entity'));
  $field = drush_get_option(array('field'));
  $search = drush_get_option(array('search'));
  $replace = drush_get_option(array('replace'));

  if (empty($entitytype)) {
    return drush_set_error('', dt('The entity value must be set!'));
  }
  if (empty($field)) {
    return drush_set_error('', dt('The field value must be set!'));
  }
  if (empty($search)) {
    return drush_set_error('', dt('The search value must be set!'));
  }
  if (empty($replace)) {
    $replace = '';
  }

  // Get entities.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', $entitytype)
      ->fieldCondition($field, 'value', $search, 'LIKE')
      // Run the query as user 1.
      ->addMetaData('account', user_load(1));

  $result = $query->execute();

  if (!empty($result[$entitytype])) {
    foreach ($result[$entitytype] as $entity_obj) {
      $entity = entity_load($entitytype, array($entity_obj->uid));
      $entity = $entity[$entity_obj->uid];
      if ($entitytype == 'user') {
        $edit = array($field => array(LANGUAGE_NONE => array(array('value' => $replace))));
        user_save($entity, $edit);
      }
    }
    drush_log(dt('Fields updated!'), 'success');
  }

}

/**
 * Update all prod users with new usernames and passwords.
 */
function drush_extra_tools_make_menus() {
  // Poets.org
  $item = array(
    'link_title' => t('test'),
    'link_path' => 'test',
    'menu_name' => 'main-menu',
    'weight' => -9,
  );
  $test = menu_link_save($item);
  $item = array(
    'link_title' => t('testsub'),
    'link_path' => 'testsub',
    'menu_name' => 'main-menu',
    'weight' => -9,
    'plid' => $test,
  );
  menu_link_save($item);

  drush_print('Menus created');
}

/**
 * Text context.
 */
function drush_extra_tools_test_context() {
  // $file = drush_get_option(array('file'));
  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'testing';
  $context->description = t('Testing Header');
  $context->tag = t('Testing Header');
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'extra_tools-staging_header' => array(
          'module' => 'extra_tools',
          'delta' => 'staging_header',
          'region' => 'content',
          'weight' => '-25',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  if (context_save($context)) {
    drush_print(dt('Success! Staging Header Enabled'));
  }
}

/**
 * Authorize tokens.
 */
function drush_extra_tools_authorize_tokens() {
  $file = drush_get_option(array('file'));
  $error = '';
  $count = 0;
  $default = 0;
  $values = array();

  $fp = fopen($file, 'r') or die("can't open file");
  // Get all the data from the csv.
  $query = db_insert('commerce_cardonfile')
    ->fields((array(
      'uid',
      'payment_method',
      'instance_id',
      'remote_id',
      'card_type',
      'card_name',
      'card_number',
      'card_exp_month',
      'card_exp_year',
      'status',
      'created',
      'changed',
      'instance_default',
    )));

  while ($csv_line = fgetcsv($fp)) {
    $user = user_load_by_mail($csv_line[0]);
    $import = TRUE;
    if (!empty($user)) {
      $result = db_select('commerce_cardonfile', 'cof')
        ->fields('cof', array('card_id', 'remote_id', 'instance_default'))
        ->condition('uid', $user->uid)
        ->execute();
      if (!empty($result)) {
        foreach ($result as $row) {
          if ($row->remote_id == trim($csv_line[1]) . '|' . trim($csv_line[2])) {
            $import = FALSE;
            break;
          }
        }
      }
      if ($import) {
        $count += 1;
        $year = trim($csv_line[6]);
        $values[] = array(array(
          'uid' => $user->uid,
          'payment_method' => 'authnet_aim',
          'instance_id' => 'authnet_aim|commerce_payment_authnet_aim',
          'remote_id' => trim($csv_line[1]) . '|' . trim($csv_line[2]),
          'card_type' => trim($csv_line[3]),
          'card_name' => '',
          'card_number' => trim($csv_line[4]),
          'card_exp_month' => trim($csv_line[5]),
          'card_exp_year' => trim($year),
          'status' => 1,
          'created' => time(),
          'changed' => time(),
          'instance_default' => $default),
        );
      }
    }
    else {
      $error .= is_null($csv_line[0]) ? 'null' : $csv_line[0] . ', ';
    }
  }
  if ($error != '') {
    $error = 'The following users emails weren\'t found' . $error;
  }

  $error = $count . ' cards were imported. ' . $error;

  foreach ($values as $record) {
    $query->values($record);
  }
  $return = $query->execute();
  fclose($fp) or die("can't close file");

  if ($return) {
    drush_print(dt('Success! The cards have been imported. ' . $error));
  }
  else {
    drush_print(dt('Nothing imported '));
  }
}

/**
 * Update all or (optionally) certain nodes.
 */
function drush_extra_tools_update_nodes() {
  // Get the supplied arguments.
  $action = drush_get_option(array('action'));
  $nids = drush_get_option(array('nids'));

  // List the possible actions.
  $possible_actions = _extra_tools_update_nodes_options();
  // If an invalid action has been supplied, inform the user.
  if ($action == '' || !in_array($action, $possible_actions)) {
    drush_set_error(t('You must supply a valid action e.g. --action=[action]. Possible actions include: @actions.', array('@actions' => implode(', ', $possible_actions))));
  }

  // Start the query to retrieve the nodes.
  $nodes = db_select('node', 'n')
    ->fields('n', array('nid'));

  // If nids have been provided, make sure we only update them.
  if ($nids != '') {
    $nids_array = explode(', ', $nids);
    $nodes = $nodes->condition('nid', $nids_array, 'in');
  }

  // Finalise the query.
  $nodes = $nodes->execute()
    ->fetchCol();

  // Determine what needs to be done to the node (determined by the action).
  $options = array();
  switch ($action) {
    case 'publish':
      $options['status'] = 1;
      break;

    case 'unpublish':
      $options['status'] = 0;
      break;
  }

  // In order to use the node_mass_update() function we need to include the file it resides in.
  module_load_include('inc', 'node', 'node.admin');

  // Finally execute the update.
  node_mass_update($nodes, $options);
}
