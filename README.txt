Extra Tools for Drupal 7.x
==========================
This module provides extra functionality for Drupal.


Installation
============
This is a standard and simple module. Place the module in your
sites/all/modules folder, then go to the modules page and enable it.
Full instructions can be found on Drupal.org: https://www.drupal.org/documentation/install/modules-themes/modules-7


Requirements
============
None


Usage
=====

User functions (admin/config/development/extra_tools/user)
--------------
* Reset emails: Update all users to have a new email address incremented with a number, eg test+1@example.com. This is useful when moving a production db back to dev for testing.
* Update passwords: Update all user passwords.

Node functions (admin/config/development/extra_tools/node)
--------------
* Export nodes: Export nodes to a Drupal Extra Tools export file.
* Import nodes: Import nodes from a Drupal Extra Tools export file.

Admin functions (admin/config/development/extra_tools/admin)
---------------
* View PHP Info page.
* Query the database through the UI.

Drush commands
--------------
* Export nodes
* Import nodes
* Create menus during site migration
* Update a field value on a specified entity
* Import a test context
* Reset emails
* Update nodes
* Update passwords
* For a full list, issue the drush command "drush help" and locate the extra_tools section.


Maintainers
===========
- kafmil (Kurt Foster)
- marc.groth (Marc Groth)
